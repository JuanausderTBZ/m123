# M123 ePortfolio

Hier folgen meine aufgaben zum Modul 123 

## Inhaltsverzeichnis

### DHCP

[DHCP-Filius](/DHCP/Filius/README.md)
Hier hab ich mit filius einen DHCP-Server konfiguriert

[DHCP-Ubuntu](/DHCP/Ubuntu/LAB.md)
Das ist ein Auftrag wo man einen DHCP _Server mit ubuntu aufsetzt.

### DNS
Dies ist ein Auftrag wo man ein DNS Server auf Windows 2022 erstellt.

[DNS-Windows](/DNS/readme.md)
