# Ubuntu Linux Installation und Konfiguration eines DHCP-Servers

1. **ISO herunterladen:**
   - Zuerst die ISO-Datei herunterladen.
   - Bei der website bei diesem link

   ![Ubuntu Download](./Images/ubuntuiso.png)

2. **Installation in VMware Workstation:**
   - In VMware Workstation create a new workstation um eine neue workstation zu erstellen.
![vmworksation](./Images/vmworkstation.png)

1. **Sprache auswählen:**
   - Bei der Sprachauswahl die gewünschte Sprache auswählen.
![Ubuntu Download](./Images/Ubuntu2%20(2).png)

1. **Systemupdates überspringen:**
   - Falls keine Updates erforderlich sind, können diese übersprungen werden.

2. **Tastaturlayout einstellen:**
   - Das Tastaturlayout auf Switzerland ändern.

3. **Ubuntu Server installieren:**
   - Die Installation mit "Ubuntu Server" oben auswählen und unten auf "Erledigt" klicken.

4. **IP-Adresse bestätigen:**
   - Die angezeigte IP-Adresse bestätigen und auf "Weiter" klicken.

5. **Proxy-Anfrage überspringen:**
   - Falls eine Proxy-Anfrage erscheint, diese einfach überspringen.

   ![Proxy](./Images/Mirror.png)

6. **Partitionierung überspringen:**
   - Die Partitionierung kann übersprungen oder nach Bedarf angepasst werden.

   ![Partition](./Images/Partition.png)

7.  **Systemzusammenfassung akzeptieren:**
    - Den Text zur Systemzusammenfassung akzeptieren.

8.  **Profil erstellen:**
    - Ein Profil erstellen, z. B. Benutzername: admin1, DHCP, Passwort: admin.

9.  **Upgrade zu Ubuntu Pro überspringen:**
    - Die Option "Upgrade to Ubuntu Pro" vorerst überspringen.

10. **SSH-Anfrage überspringen:**
    - Eine etwaige SSH-Anfrage einfach überspringen.

11. **Unterstützte Server-Snaps auswählen:**
    - Gegebenenfalls unterstützte Server-Snaps auswählen oder überspringen.

12. **Neustart nach der Installation:**
    - Nach Abschluss erfolgt ein Neustart.

13. **GRUB-Bootloader wählen:**
    - Den GRUB-Bootloader öffnen und Ubuntu auswählen.

14. **Anmeldung und Systemupdate:**
    - Anmelden und mit `sudo apt-get update` das System aktualisieren.

15. **DHCP-Server-Konfiguration:**
    - Die DHCP-Konfiguration mit `sudo apt install isc-dhcp-server -y` installieren.

16. **Netzwerkkonfiguration überprüfen:**
    - Die Netzwerkkonfiguration mit `ip a` überprüfen, insbesondere die ens-Schnittstellen.

    ![Netzwerkkonfiguration](./Images/ens.png)

17. **DHCP-Server-Interface konfigurieren:**
    - Die Konfiguration des DHCP-Server-Interfaces mit
    - Zwischen denn gänsefüsslein ens eingeben und die nummer vom vorheriger schritt. `sudo nano /etc/default/isc-dhcp-server/` anpassen.

    ![Interface Konfiguration](./Images/ensconfig.png)

    - Die Datei speichern (CTRL + X, dann Y und Enter).

19. **authorotiv** 
    Vor dem authorativ das hashtag entfernen
    ![Authorativ](./Images/ipauthorativ.png)

20. **IP-Bereich und Optionen konfigurieren:**
    - Die IP Range und Optionen im DHCP-Server mit `sudo nano /etc/default/isc-dhcp-server/` festlegen.

    ![IP Range](./Images/iprange.png)

    - Die Datei speichern.

21. **Statische IP-Adresse konfigurieren:**
    - Die statische IP-Adresse im Netplan-Konfigurationsfile mit `sudo nano /etc/netplan/00-installer-config.yaml` einstellen.

    ![Netplan YAML](./Images/yaml.png)

    - Die Datei speichern.

22. **DHCP-Server testen:**
    - Den DHCP-Server mit `sudo systemctl start isc-dhcp-server` starten.
    - Den Autostart aktivieren: `sudo systemctl enable isc-dhcp-server`.
    - Den Status überprüfen: `sudo systemctl status isc-dhcp-server`.

    ![DHCP-Status](./Images/dhcpstatus.png)

## Troubleshooting:

1. Hab ich probiert denn server 5mal gleich neu zu installieren in dem ich die vm gelöscht habe und  habe es jedasmal getestet bis zum punk nach dem start wo ich den status getest habe bin ich nicht weiter gekommen.
2. Ich habe auch probiert mit der 1 oder 2 netzkartem zu instalieren.
3. Hab ich jedesmal die datein abgechekt und auch noch ein reboot.
4. Was ich auch noch probiert habe ist ein bischen mit chat gpg die errors durch zu gehen der hat mir emphohlen in die logs zu gehen das hat mir nicht viel weiter gebracht.
5. Mit den Logs bin ich darauf gekommen das das problem vieleicht an der configuration lieht.
6. Die Tutorials wo ich benütz habe: https://www.linuxtechi.com/how-to-configure-dhcp-server-on-ubuntu/ 
7. https://gitlab.com/ser-cal/m123-lernprodukt/-/blob/main/01_DHCP/README.md
8. https://gal.vin/posts/2023/ubuntu-static-ip/
9.  https://www.youtube.com/watch?v=Xe61fxI8pQQ
10. und noch aaron seine https://teams.microsoft.com/l/message/19:827062e6-85f3-4972-b72d-7687d0bfb0dc_a3e3360d-cbfe-4447-baa9-2964a0bc9d65@unq.gbl.spaces/1702839006792?context=%7B%22contextType%22%3A%22chat%22%7D