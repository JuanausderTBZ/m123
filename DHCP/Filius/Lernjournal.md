# M123

DHCP

Auftrag2:

[show running-config](https://gitlab.com/alptbz/m123/-/blob/main/05_DHCP/DHCP_PacketTracer/01_DHCP.md?ref_type=heads#1-router-konfiguration-auslesen)

Zuerrst auf dem Router DRauf clicken danach auf cli enable zum admin bekommen.

---

Fraage: 1:
Für welches Subnetz ist der DHCP Server aktiv?

Für Subnetz 255.255.255.0

---

Fraage2:
Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)




|IP-Adresse| MAC-Adresse |
| ---|-------|
|192.168.30.31 | 00D0.BC52.B29B|
|192.168.30.35  |  0050.0F4E.1D82|
|192.168.30.36  | 0009.7CA6.4CE3|
|192.168.30.33  | 00E0.8F4E.65AA|
|192.168.30.32  | 0007.ECB6.4534|
|192.168.30.34  | 0001.632C.3508|

---

Fraage3:
In welchem Range vergibt der DHCP-Server IPv4 Adressen?
Von 192.168.30.1 bis 192.168.30.254

---

Fraage4:
Was hat die Konfiguration ip dhcp excluded-address zur Folge?
Dies hat zur folge das Fixe Ip adreessen vergeben werde für Drucker,Server,Kühlschrank, Switch layerect

---

Fraage5:

Total: 254 aber 2 exluded IP_Adressen.

---

## Dora-DHCP



Fraage 1:
Welcher OP-Code hat der DHCP-Offer?
OP:0x2 

---

Fraage 2:
Welcher OP-Code hat der DHCP-Request?
OP:0x1

---

Fraage3:
Welcher OP-Code hat der DHCP-Acknowledge?
OP:0x2

---

Fraage4:
An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?
Am Broadcast 255.255.255.255

---

Fraage6:
An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?
An der Broadcast MAc Adreesse: (ff-ff-ff-ff-ff-ff)

---

Fraage 7:
Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?
Da der switch für den daten transfer Für einen Broadcast hier ist.

---

Fraage 8:
Welche IPv4-Adresse wird dem Client zugewiesen?
192.168.30.1

---

### Screenshot des DHCP-Discovers PDUs:

![DHCP-Discovers](./images/DHCP-Discovers.png)

### Screenshot des DHCP-Offers PDUs

![DHCP-Offers](./images/DHCP-Offers.png)

### Screenshot des DHCP-Request PDUs

![DHCP-Request](./images/DHCP-Request.png)

### Screenshot des DHCP-Acknowledge PDUs

![DHCP-Acknowledge](./images/DHCP-Acknowledge.png)

## Netzwerk umkonfigurieren

Die IP Adreesse vom server ist 192.168.30.240

Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse vom Interface FastEthernet

![Fastethernet](./images/fastethernet.png)

Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse des Gateways

![Gateway](./images/Gateways.png)

Screenshot der Webseite des Servers auf einem der PCs inkl. sichtbarer IPv4-Adresse des Servers

![Website](./images/Serverwebsite.png)