[TOC]

# Konfiguration eines DHCP-Servers auf Filius

## Netzwerktopologie:

Das Netzwerk verwendet eine Stern-Topologie mit einem DHCP-Server und drei Clients.

**Umgebung:**
- 1x DHCP-Server
- Switch/WLAN
- Client 1: IP - DHCP
- Client 2: IP - DHCP
- Client 3: IP - 192.168.0.50

![Netzwerktopologie](./images/Bild1.png)

## Zielsetzung:

Das Ziel besteht darin, zwei dynamische DHCP-IP-Adressen und eine feste, ausgewählte IP-Adresse zu vergeben.

**Umgebung:**
- 1x DHCP-Server
- Switch/WLAN
- Client 1: IP - DHCP
- Client 2: IP - DHCP
- Client 3: IP - 192.168.0.50

| Umgebung | IP-Adresse |
| --- | ----------: |
| DHCP-Server | - |
| Switch | - |
|Client 1| IP - DHCP|
|Client 2| IP - DHCP|
|Client 3| IP - 192.168.0.50|

## Konfiguration der Clients/Laptops:

1. Klicken Sie auf die beiden Clients/Laptops.
2. Wählen Sie "DHCP nur Konfiguration verwenden", damit der Client/Laptop die DHCP-Konfiguration annimmt. Dies gilt nur für die beiden Clients/Laptops, die die IP-Adresse vom DHCP-Server beziehen sollen.

![Clients](./images/Bild2.jpg)

## Festlegen der IP-Adressbereich (Range):

Im DHCP-Server-Fenster geben Sie die vorgegebene IP-Adressen-Reichweite ein. Die Adress-Untergrenze ist die niedrigste IP-Adresse, und die Adress-Obergrenze ist die höchste IP-Adresse in diesem Bereich. In diesem Fall ist die Untergrenze 192.168.0.150 und die Obergrenze 192.168.0.170.

![DHCP](./images/Bild3.jpg)

## Vergeben statischer IP-Adressen:

1. Gehen Sie im DHCP-Server-Fenster oben auf "Statische Adresszuweisung".
2. Geben Sie die MAC-Adresse des gewünschten Geräts und die zugehörige feste IP-Adresse ein.
3. Klicken Sie auf "Hinzufügen", dann auf "OK", um die Zuweisung zu bestätigen.

![DHCP-MAC-Adressen](./images/Bild5.jpg)

4. Klicken Sie auf "Hinzufügen", bestätigen Sie mit "OK" und lassen Sie den DHCP-Server laufen, bis die IP-Adressen erfolgreich hinzugefügt wurden.

## IP-Test:

Zum Schluss führen Sie einen Ping-Test von 192.168.0.150 auf 192.168.0.50 durch.

![PING-Test](./images/Bild7.jpg)
