## Fileshare

## Check

Zuerst sollte man denn Ubuntu server Pingen dafür braucht man einen Server und ein client im gleichen Netzwerk.

## Samba

Zuerst die updates machen und die Benötigten Server Applikationen Installieren.

`sudo apt update -y`

`sudo apt install samba netplan -y`

## Fire Wall

Die Fire wall komplett zu deaktivieren mit:
sudo ufw allow Samba

## Erstellen des files

um ein Ordner zu erstellen auf Terminal

`sudo mkdir /sambashare`
Denn namen bei sambashare kann man selber auswhählen.

## Benutzer 

Benutzer für samba hinzfügen

`sudo usermod -aG  sambashare $USER`

`sudo smbpassw -a $USER`

danach kommt eine anfraage um ein Passwort estellen.

## Ordner Sharen

Mit dem Gui zum Ordner Naviegieren Rechts click auf 

![Share](./Images/Screenshot%202024-01-22%20095439.png)

Hier auf Share this folder einen hacken setzen.
einen namen auswähelen und einen Hacken noch setzen bei Allow others... 
Danach auf modify share und Add the Permission um es zu bestätigen.

Zum schluss 
`sudo systemctl restart smbd`
um den Server neu zu starten

## Client

Beim Windows client auf dem Explorer gehen und bei Network `\\ IP-adresse \ Pfad`
Bei der IP-Adresse kommt die Ip vom fileserver und danach die weitere pfade bis zum gewünscten Ordner.

## Trobleshooting

Bei mir ging das nicht da irgrndwann denn Server nicht mehr anpingen konnte ich habe danach die Netzwerk einstellung angeschauten aber hier war alles gut auf der client side und dem server.

![IP-configs](./Images/IP-ubuntu.png)

![Ip-config](./Images/IP-Windows.png)

Die firewall auf beidem geräten hab ich auch schon deaktiviert.

Virtual Network Editor hab ich auch schon die settings angeschaut und mit anderen schüler vergliechen.

Ich habe auch noch das Ubuntu neu aufgesetzt und mit dem Ubuntu serrver augesetzt und auch probiert.

Danach hab ich VM Workstation Updates gemacht und das tool deinstlatiert und neu installiert.

Zum schluss habe ich noch bei der Netzkarte einen Bridge gemacht dann hat dies Geklappt.

