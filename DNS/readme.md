# Windows 2022 DNS Server Installation

[TOC]

## DNS Instalation

Die Instalation ist für Windows server 2022 EN version gedacht, vieleicht würde sie auch auf windows 2019 version funkionieren aber dies kann ich nicht bestätigen.

## Server Instalation

Hier ist es sehr wichtig die Desktop version Herunterzuladen da die Desktop version eine graphisches Interface hat und die standart version nur powershell terminal hat.

![Instalation](./image/Screenshot%202024-01-03%20222656.png)

## Server-IP

Nach der Instalation geht man auf die einstellungen beim server und ändert die IP-Adresse auf eine fixe IP

![IP-Config](./image/Statische-IP.png)

Bei mir vergib ich die 
- IP 192.168.100.8 
  
bei DNS 
- Preffered: 127.0.0.1
für locale DNS und
- Alternate:8.8.8.8
Dies ist der DNS von Google aber dies ist nicht nötig

## DNS-Instalation

Bei der windows suche sucht man nach windows Manager und dies öfnet man dan

![Server Manager](./image/Roles.png)

Hier Click man auf Add-Rolles

---

Dann kommt ein Instalationfenster auf 

![Roles](./image/installationtype1.png)

Hier Click man direkt auf Server Roles

---
Hier wählt man dan DNS Server

![Roles1](./image/Roles-dns.png)

---

Hier das Kästchen auswählen un dan einfach auf Add Features

![Roles2](./image/Roles-dns2.png)

---

Hier einfach auf Yes drücken und dan startet der Server neu wenn er dies nicht automatisch tut sollte man ihn manuel Neustarten

![Roles3](./image/Roles-dns-restart.png)

---
Nach dem Neustart der Server Manager wieder aufmachen und oben rechts auf tools und dann auf DNS

![Tools](./image/tools.png)

---
Dann Geht dieses Fenster auf 

![Lookup](./image/forward-newzone.png)

Clicke auf New Zone
Danach Kommt eine Welcome Page hier cklick man einfach auf next

---

Hier einfach auf primary zone und dan Next

![Lookup2](./image/forward-newzone2.png)

---

Hier ein irgendein gewünschter name wie der server dan heissen sollte bei mir ist dies Jetzt sota wegen der Sota GMBH

![Lookup3](./image/forward-newzone3.png)

---

Hier der Name des Erstellten files

![Lookup4](./image/forward-newzone4.png)

---

Hier kann man nach automatischen updates auswählen die verneinen wie hier.

![Lookup5](./image/forward-newzone5.png)

---

Dies sollte dan so aussehen wenn dies korrekt ist dann auf finish.

![Lookup6](./image/forward-newzone6.png)

---

Hier hgeht man anstaht forward lookup zone zu reverse lookup zone und click wieder auf new Zone

![Reverse](./image/forward-newzone.png)

---

Hier auf primary Zone un dan next

![Reverse2](./image/forward-newzone-reverse.png)

---

Hier kann man die Ip art auswählen aber wir gehen mit IPv4

![Reverse3](./image/forward-newzone-reverse2.png)

---

dies ist der file name der wird Automatisch vergeben

![Reverse4](./image/forward-newzone-reverse4.png)

---

Hier kann man nach automatischen updates auswählen die verneinen wie hier.
![Reverse5](./image/forward-newzone-reverse5.png)

---

Hier wieder alles abcheken ob alles korrekt ist und danach auf finish

![Reverse6](./image/forward-newzone-reverse6.png)

---

Hier auf den Primary Zone und dan auf next.

![Reverse7](./image/forward-newzone-reverse.png)

---

Hier auf Forward Lookup Zones sota ancklicken und dan New Host A

![Reversemail](./image/Reverselookupzoneeinrchten.png)

---

Hier neuen Host hinzufügen in dem fall für denn DNS Server
mit der IP-Adresse vom Dns und nochmals für den client mit der Ip-Adresse vom client.

![Host](./image/Host.png)

![Host2](./image/Host2.png)

---

## Forwardes

Um die Forwardes Konfigurieren auf dem Server rechtsckilcken unter dem DNS und auf Properties.

![Forward](./image/Forward.png)

---

Hier auf Forwardes Naviegieren und auf Edit clicken.

![Forward2](./image/forward2.png)

---

Hier eine beliebiege Ip-Adresse von einem DNS eingeben um ins Internet zu Kommen in meinem fall Google 8.8.8.8.


![Forward3](./image/forward3.png)

---
Wenn dies so ausieht auf Apply drücken um die ändrungen übernehmen.

![Forward4](./image/forward4.png)

Hier ist das ende vom DNS Server Jetzt zum Client.

---

## Client

Zuerst beim Client die den DNS Server Hinzufügen

![client](./image/IP-Config-Client.png)

Unten einfach Use the following Dns Server ancklicken danach die IP-Adresse vom DNS Angeben bei mir 
- Prefered :  192.168.100.8
- Alternate : 8.8.8.8
Der alternative DNS server ist von Google damit man normal browsen kann.

## Test

Als aller erstes ein Ping zum server wenn der erfolgreich ist, dann auf Powershell und dann den command nslooup benutzen nslookup ist für Anfragepaket für einen Domänennamen.

- nslookup web.sota.ch
- nslookup clien.sota.ch
- nslookup www.20min.ch

Der nslooukup für 20min ist Optional den er ist zum testen ob der forwarder funkitioniert.
Je nach DNS Host name ist es hinter dem lookup anderst.
Wenn dies erfolgreich ist sollte es wie untem im Bild anzeigen.

---

![Test](./image/test1.png)

Unter dem lookups sollte dan die IP-Adresse vom DNS Server sein.  

---
